import {createMuiTheme, responsiveFontSizes} from '@material-ui/core/styles';

let theme = createMuiTheme({
    palette: {
        primary: {
            main: '#163E2D',
            light: '#F1F6F4',
        },
        secondary: {
            main: '#EE7D11',
            light: '#FEF2E0',
        },
        error: {
            main: '#B0001F',
            light: '#FDECEA',
        },
        klein: {
            main: '#0008f4',
            light: '#0045f4',
        }
    },
});

theme = responsiveFontSizes(theme);

export default theme;
