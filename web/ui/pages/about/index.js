import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import {makeStyles} from '@material-ui/core/styles';
import {Typography} from '@material-ui/core';
import { useRouter } from 'next/router';

const useStyles = makeStyles( theme => ({
  klein: {
    color: theme.palette.klein.main,
  },
}));

export default function Home() {
  const url = `${process.env.REACT_APP_API_ROOT}`
  const classes = useStyles();
  const router = useRouter();

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Typography variant='h3' className={classes.klein}>
         I'm happy to see you here ʕ •ᴥ•ʔ
        </Typography>
        <Typography variant='h5'>
         As with all unknown things, <strong>this is going to be an adventure</strong>
        </Typography>
        <Typography variant='h5'>
         - but no worries, since you already worked up the courage to be here, you'll be just fine. 
        </Typography>
        <Typography variant='h5'>
         So, want to learn code but unsure how/where to start? From here you can go quickly and full stack.
        </Typography>
        <Typography variant='h5'>
         <strong>What to do now? Take a break and relax!</strong> You have just done something really huge.
        </Typography>
        
      </main>

      <footer className={styles.footer}>
        <a
          href="https://procnedc.github.io/resume/"
          target="_blank"
          rel="Daniela Colombo"
          className={classes.klein}
        >
          Made with ♥ by DC
        </a>
      </footer>
    </div>
  )
}
