import Head from 'next/head'
import styles from '../styles/Home.module.css'
import {makeStyles} from '@material-ui/core/styles';
import {Typography} from '@material-ui/core';
import { useRouter } from 'next/router';

const useStyles = makeStyles( theme => ({
  klein: {
    color: theme.palette.klein.main,
  },
}));

export default function Home() {
  const url = `${process.env.REACT_APP_API_ROOT}`
  const classes = useStyles();
  const router = useRouter();

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Typography variant='h2' className={classes.klein}>
          Welcome to the <a href="https://gitlab.com/danicolombo/starter-kit" target="_blank">Starter kit!</a>
        </Typography>
        
        <span onClick={() => router.push('/about')}>Click me</span>

        <p className={styles.description}>
          Get started by editing{' '}
          <code className={styles.code}>pages/index.js</code>
        </p>

        <div className={styles.grid}>
          <a href={`${url}/admin`} className={styles.card}>
            <h3>Django backend &rarr;</h3>
            <p>Use the most powerful part of Django: the admin interface.</p>
          </a>

          <a href={`${url}/graphql`} className={styles.card}>
            <h3>GraphQL &rarr;</h3>
            <p>A query language for your Django backend.</p>
          </a>

          <a
            href="https://material-ui.com/"
            className={styles.card}
            >
            <h3>Material ui &rarr;</h3>
            <p>React components for faster and easier web development.</p>
          </a>

          <a
            href="https://nextjs.org/"
            className={styles.card}
          >
            <h3>Next.js &rarr;</h3>
            <p>
              React framework. Hybrid static and server side rendering.
            </p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://procnedc.github.io/resume/"
          target="_blank"
          rel="Daniela Colombo"
          className={classes.klein}
        >
          Made with ♥ by DC
        </a>
      </footer>
    </div>
  )
}
