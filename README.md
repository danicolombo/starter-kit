# React + Django + PostgreSQL + Next.js + Material ui + GraphQL + Docker Starter Kit

## Development Setup (Docker)

### Requirements

- Docker
- Docker Compose
- NodeJS and NPM

### Execute

```cp .env .env.dev``` 


Update values as needed


```npm install --prefix ./web/ui/```

```docker-compose up -d --build```

```docker exec -it web python ./manage.py migrate```

```docker exec -it web python ./manage.py createsuperuser```

```docker-compose exec web npm install --prefix ./ui/```

```docker-compose exec web npm run dev --prefix ./ui/```


```
Compiled successfully!

ready - started server on 0.0.0.0:3000, url: http://localhost:3000
```
